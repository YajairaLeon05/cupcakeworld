package com.ymp.cupcake.controller;

import com.ymp.cupcake.model.business.SucursalBusiness;
import com.ymp.cupcake.model.business.UsuarioBusiness;
import com.ymp.cupcake.model.entities.Direccion;
import com.ymp.cupcake.model.entities.Sucursal;
import com.ymp.cupcake.model.entities.Usuario;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("/")
public class MainCtrl {
    
    @Autowired
    private UsuarioBusiness usuarioBusiness;
    
    @Autowired
    private SucursalBusiness sucursalBusiness;

    @GetMapping("/Login")
    public String login(){
        return "login";
    }

    @GetMapping("/")
    public String index(HttpSession httpSession){
        SecurityContext securityContext = SecurityContextHolder.getContext();
        Authentication authentication = securityContext.getAuthentication();
        if (authentication != null){
            Object principal = authentication.getPrincipal();
            UserDetails user = principal instanceof UserDetails ? (UserDetails) principal : null;
            if(user != null){
                String auth = user.getAuthorities().toString();
                if(auth != null){
                    
                    if(httpSession.getAttribute("usuario") == null){
                        httpSession.setAttribute("usuario", usuarioBusiness.obtenerUsuarioDeSesion());
                    }
                    
                    switch (auth){
                        case "[ROLE_ADMIN]":
                            return "redirect:/Admin";
                        case "[ROLE_EMPLEADO]":
                            return "redirect:/Empleado";   
                    }
                    return "index";
                }else{
                    return "index";
                }
            }else{
                return "index";
            }
        }else{
            return "index";
        }
    }
    
    /* Registro */
    
    @GetMapping("/Registrarse")
    public String registrarse(){
        return "registarse";
    }
    
    @PostMapping("/Registrarse")
    public String registrarCliente(Usuario usuario, Direccion direccion, @RequestParam int idSucursal,RedirectAttributes redirectAttributes){
        Sucursal sucursal = sucursalBusiness.buscarPorId(idSucursal);
        
        if(sucursal != null){
            if(usuarioBusiness.registrarUsuario(usuario, direccion, sucursal)){
                redirectAttributes.addAttribute("mensaje", "Registro correcto");
            }
            else{
                redirectAttributes.addAttribute("mensaje", "Ocurrio un error al hacer el registro");
            }
        }
        else{
            redirectAttributes.addAttribute("mensaje", "La sucursal no fue encontrada");
        }
        return "redirect:/";
    }
  
    @GetMapping("/403")
    public String error403(){
        return "error403";
    }





}
