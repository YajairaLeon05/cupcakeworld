/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ymp.cupcake.controller;

import com.ymp.cupcake.model.business.CategoriaBusiness;
import com.ymp.cupcake.model.business.ProductoBusiness;
import com.ymp.cupcake.model.business.SucursalBusiness;
import com.ymp.cupcake.model.business.UsuarioBusiness;
import com.ymp.cupcake.model.business.VentaBusiness;
import com.ymp.cupcake.model.entities.Categoria;
import com.ymp.cupcake.model.entities.Direccion;
import com.ymp.cupcake.model.entities.Producto;
import com.ymp.cupcake.model.entities.Subcategoria;
import com.ymp.cupcake.model.entities.Sucursal;
import com.ymp.cupcake.model.entities.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author pedro
 */
@Controller
@RequestMapping("/")
public class AdminCtrl {
    
    @Autowired
    private CategoriaBusiness categoriaBusiness;
    
    @Autowired
    private UsuarioBusiness usuarioBusiness;
    
    @Autowired
    private ProductoBusiness productoBusiness;
    
    @Autowired
    private SucursalBusiness sucursalBusiness;
    
    @Autowired
    private VentaBusiness ventaBusiness;
    
    
    
    
    @RequestMapping("/Admin")
    public String page(Model model) {
        return "adminIndex";
    }
    
    /* Modulo de usuarios */
    
    @GetMapping("/Admin/Usuarios")
    public String gestionarUsuarios(Model model){
        model.addAttribute("usuarios", usuarioBusiness.todosLosUsuarios());
        return "adminUsuarios";
    }
    
    @PostMapping("/Admin/Usuarios/AgregarUsuario")
    public String agregarUsuario(Usuario usuario, Direccion direccion, @RequestParam int idSucursal, RedirectAttributes redirectAttributes){
        Sucursal sucursal = sucursalBusiness.buscarPorId(idSucursal);
        
        if(sucursal != null){
            if(usuarioBusiness.registrarUsuario(usuario, direccion, sucursal)){
                redirectAttributes.addAttribute("mensaje", "Registro correcto");
            }
            else{
                redirectAttributes.addAttribute("mensaje", "Ocurrio un error al hacer el registro");
            }
        }
        else{
            redirectAttributes.addAttribute("mensaje", "La sucursal no fue encontrada");
        }
        
        return "redirect:/Admin/Usuarios";
    }
    
    @PostMapping("/Admin/Usuarios/BajaUsuario")
    public String bajaUsuario(Usuario usuario){
        usuarioBusiness.desactivarUsuario(usuario);
        return "redirect:/Admin/Usuarios";
    }
    
    @PostMapping("/Admin/Usuarios/EditarUsuario")
    public String editarUsuario(Usuario usuario, Direccion direccion){
        return "redirect:/Admin/Usuarios";
    }
    
    /* Modulo de productos */
    
    @GetMapping("/Admin/Productos")
    public String gestionarProductos(Model model){
        model.addAttribute("productos", productoBusiness.todosLosProductos());
        return "adminProductos";
    }
    
    @GetMapping("/Admin/Productos/TraerProducto/{id}")
    public ResponseEntity<Producto> traerProducto(@PathVariable Number id){
        return ResponseEntity.ok().body(productoBusiness.traerProducto(id));
    }
    
    @PostMapping("/Admin/Productos/AgregarProducto")
    public String agregarProducto(Producto producto){
        productoBusiness.guardarProducto(producto);
        return "redirect:/Admin/Productos";
    }
    
    @PostMapping("/Admin/Productos/EliminarProducto/{id}")
    public String eliminarProducto(@PathVariable Number id){
        productoBusiness.eliminarProducto(id);
        return "redirect:/Admin/Productos";
    }
    
    @PostMapping("/Admin/Productos/EditarProducto")
    public String editarProducto(Producto producto){
        productoBusiness.guardarProducto(producto);
        return "redirect:/Admin/Productos";
    }
    
    /* Modulo de sucursales*/
    
    @GetMapping("/Admin/Sucursales")
    public String gestionarSucursales(Model model){
        model.addAttribute("sucursales", sucursalBusiness.todasLasSucursales());
        return "adminSucursales";
    }
    
    @GetMapping("/Admin/Sucursales/TraerSucursal/{id}")
    public ResponseEntity<Sucursal> traerSucursal(@PathVariable Number id){
        return ResponseEntity.ok().body(sucursalBusiness.buscarPorId(id));
    }
    
    @PostMapping("/Admin/Sucursales/AgregarSucursal")
    public String agregarSucursal(Sucursal sucursal){
        sucursalBusiness.agregarSucursal(sucursal);
        return "redirect:/Admin/Sucursales";
    }
    
    @PostMapping("/Admin/Sucursales/EliminarSucursal/{id}")
    public String eliminarSucursal(@PathVariable Number id){
        productoBusiness.eliminarProducto(id);
        return "redirect:/Admin/Sucursales";
    }
    
    @PostMapping("/Admin/Sucursales/EditarProducto")
    public String editarSucursal(Sucursal sucursal){
        sucursalBusiness.editarSucursal(sucursal);
        return "redirect:/Admin/Sucursales";
    }
    
    /* Modulo de ventas */
    
    @GetMapping("/Admin/Ventas")
    public String gestionarVentas(Model model){
        model.addAttribute("ventas", ventaBusiness.todasLasVentas());
        return "adminVentas";
    }
    
    /* Modulo de categorias */
    
    @GetMapping("/Admin/Categorias")
    public String gestionarCategorias(Model model){
        model.addAttribute("categorias", categoriaBusiness.todasLasCategorias());
        return "adminCategorias";
    }
    
    @GetMapping("/Admin/Categorias/Categoria/{id}")
    public String verSubcategorias(Model model, @PathVariable Number id){
        model.addAttribute("subcategorias",categoriaBusiness.subCategoriasdeCategoria(id));
        return "categoria";
    }
    
    
    @PostMapping("/Admin/Categorias/NuevaCategoria")
    public String agregarCategoria(Categoria categoria){
        categoriaBusiness.guardarCategoria(categoria);
        return "/Admin/Categorias";
    }
    
    @PostMapping("/Admin/Categorias/NuevaSubcategoria")
    public String agregarSubcategoria(Subcategoria subcategoria){
        categoriaBusiness.guardarSubCategoria(subcategoria);
        return "/Admin/Categorias";
    }
    
}
