/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ymp.cupcake.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author pedro
 */
@Controller
@RequestMapping("/")
public class ClienteCtrl {
    
    @RequestMapping("/Cliente")
    public String page(Model model) {
        model.addAttribute("attribute", "value");
        return "";
    }
    
}
