/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ymp.cupcake.model.business;

import com.ymp.cupcake.model.entities.Categoria;
import com.ymp.cupcake.model.entities.Subcategoria;
import com.ymp.cupcake.model.repository.CategoriaRepository;
import com.ymp.cupcake.model.repository.SubcategoriaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author pedro
 */
@Component
public class CategoriaBusiness {
    
    @Autowired
    private CategoriaRepository categoriaRepository;
    
    @Autowired
    private SubcategoriaRepository subcategoriaRepository;
    
    
    public Iterable<Categoria> todasLasCategorias(){
        return categoriaRepository.findAll();
    }
    
    public Iterable<Subcategoria> subCategoriasdeCategoria(Number id){
        return subcategoriaRepository.findAllByIdCategoriaFkEquals(id);
    }
    
    public void guardarCategoria(Categoria categoria){
        categoriaRepository.save(categoria);
    }
    
    public void guardarSubCategoria(Subcategoria subcategoria){
        subcategoriaRepository.save(subcategoria);
    }
    
    public void eliminarSubcategoria(Number id){
        subcategoriaRepository.deleteById(id);
    }
    
    
}
