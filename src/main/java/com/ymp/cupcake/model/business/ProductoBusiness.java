/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ymp.cupcake.model.business;

import com.ymp.cupcake.model.entities.Producto;
import com.ymp.cupcake.model.entities.ProductoSucursal;
import com.ymp.cupcake.model.entities.Sucursal;
import com.ymp.cupcake.model.repository.ProductoRepository;
import com.ymp.cupcake.model.repository.ProductoSucursalRepository;
import com.ymp.cupcake.model.repository.SucursalRepository;
import java.math.BigDecimal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author pedro
 */
@Component
public class ProductoBusiness {
    
    @Autowired
    private ProductoRepository productoRepository;
    
    @Autowired
    private SucursalRepository sucursalRepository;
    
    @Autowired
    private ProductoSucursalRepository productoSucursalRepository;
    
    public Iterable<Producto> todosLosProductos(){
        return productoRepository.findAll();
    }
    
    public boolean registrarProductoEnSucursal(Producto producto, Sucursal sucursal, BigDecimal precio, int cantidad){
        try {
            ProductoSucursal productoSucursal = new ProductoSucursal();
            
            productoSucursal.setIdProducto(producto);
            productoSucursal.setIdSucursal(sucursal);
            productoSucursal.setCantidad(cantidad);
            productoSucursal.setPrecio(precio);
            productoSucursal.setEstatus(Boolean.TRUE);
            
            productoSucursalRepository.save(productoSucursal);
            
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        
        
        return true;
    }
    
    public void guardarProducto(Producto producto){
        productoRepository.save(producto);
    }
    
    public void desactivarProducto(Number id){
        Producto producto = traerProducto(id);
        if(producto != null){
            producto.setEstatus(false);
            productoRepository.save(producto);
        }
    }
    
    public void eliminarProducto(Number id){
        productoRepository.deleteById(id);
    }
    
    public Producto traerProducto(Number id){
        return productoRepository.findById(id).orElse(null);
    }
    
}
