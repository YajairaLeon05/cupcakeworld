/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ymp.cupcake.model.business;

import com.ymp.cupcake.model.entities.Venta;
import com.ymp.cupcake.model.repository.VentaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author pedro
 */
@Component
public class VentaBusiness {
    
    @Autowired
    private VentaRepository ventaRepository;
    
    public Iterable<Venta> todasLasVentas(){
        return ventaRepository.findAll();
    }
        
}
