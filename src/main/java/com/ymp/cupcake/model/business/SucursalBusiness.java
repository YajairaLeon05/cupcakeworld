/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ymp.cupcake.model.business;

import com.ymp.cupcake.model.repository.SucursalRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.ymp.cupcake.model.entities.Sucursal;

/**
 *
 * @author pedro
 */
@Component
public class SucursalBusiness {
    
    @Autowired
    private SucursalRepository sucursalRepository;
    
    
    public Iterable<Sucursal> todasLasSucursales(){
        return sucursalRepository.findAll();
    }
    
    public boolean existeSucursal(Number id){
        return sucursalRepository.existsById(id);
    }
    
    public Sucursal buscarPorId(Number id){
        return sucursalRepository.findById(id).orElse(null);
    }
    
    public Sucursal agregarSucursal(Sucursal sucursal){
        return sucursalRepository.save(sucursal);
    }
    
    public Sucursal editarSucursal(Sucursal sucursal){
        return sucursalRepository.save(sucursal);
    }
    
}
