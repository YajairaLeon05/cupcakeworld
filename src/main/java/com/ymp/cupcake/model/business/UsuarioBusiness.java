/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ymp.cupcake.model.business;

import com.ymp.cupcake.model.entities.Direccion;
import com.ymp.cupcake.model.entities.Sucursal;
import com.ymp.cupcake.model.entities.Usuario;
import com.ymp.cupcake.model.entities.UsuarioDireccion;
import com.ymp.cupcake.model.entities.UsuarioSucursal;
import com.ymp.cupcake.model.repository.DireccionRepository;
import com.ymp.cupcake.model.repository.UsuarioRepository;
import com.ymp.cupcake.model.repository.UsuarioSucursalRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

/**
 *
 * @author pedro
 */
@Component
public class UsuarioBusiness {
    
    @Autowired
    private UsuarioRepository usuarioRepository;
    
    @Autowired
    private DireccionRepository direccionRepository;
    
    @Autowired
    private UsuarioSucursalRepository usuarioSucursalRepository;
    
    public Usuario buscarUsuario(String username){
        return usuarioRepository.findByUsernameEquals(username).orElse(null);
    }
    
    public Iterable<Usuario> todosLosUsuarios(){
        return usuarioRepository.findAll();
    } 
    
    public Usuario obtenerUsuarioDeSesion(){
        SecurityContext securityContext = SecurityContextHolder.getContext();
        Authentication authentication = securityContext.getAuthentication();
        if (authentication != null) {
            Object principal = authentication.getPrincipal();
            UserDetails user = principal instanceof UserDetails ? (UserDetails) principal : null;
            return usuarioRepository.findByUsernameEquals(user.getUsername()).orElse(null);
        }
        return null;
    }
    
    public boolean registrarUsuario(Usuario usuario, Direccion direccion, Sucursal sucursal){
        try{
        usuario = usuarioRepository.save(usuario);
        direccion = direccionRepository.save(direccion);
        
        UsuarioDireccion usuarioDireccion = new UsuarioDireccion();
        usuarioDireccion.setIdDireccion(direccion);
        usuarioDireccion.setIdUsuario(usuario);
        usuarioDireccion.setEstatus(true);
        
        
        UsuarioSucursal usuarioSucursal = new UsuarioSucursal();
        usuarioSucursal.setIdSucursal(sucursal);
        usuarioSucursal.setIdUsuario(usuario);
        usuarioSucursal.setEstatus(Byte.parseByte("1"));
        
        usuarioSucursalRepository.save(usuarioSucursal);
        }
        catch(Exception e){
            e.printStackTrace();
            return false;
        }
        
        return true;
    }
    
    public Usuario guardarUsuario(Usuario usuario){
        return usuarioRepository.save(usuario);
    }
  
    
    public Usuario buscarPorId(Number id){
        return usuarioRepository.findById(id).orElse(null);
    }
    
    public void borrarUsuario(Usuario usuario){
        usuarioRepository.delete(usuario);
    }
    
    public void desactivarUsuario(Usuario usuario){
        if(usuarioRepository.existsById(usuario.getId())){
            usuario.setEstatus(false);
            usuarioRepository.save(usuario);
        }
    }
    
    public void borrarPorId(Number id){
        usuarioRepository.deleteById(id);
    }
    
}
