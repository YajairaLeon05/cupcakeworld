/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ymp.cupcake.model.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author pedro
 */
@Entity
@Table(name = "direccion")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Direccion.findAll", query = "SELECT d FROM Direccion d")
    , @NamedQuery(name = "Direccion.findById", query = "SELECT d FROM Direccion d WHERE d.id = :id")
    , @NamedQuery(name = "Direccion.findByCalle", query = "SELECT d FROM Direccion d WHERE d.calle = :calle")
    , @NamedQuery(name = "Direccion.findByNoExt", query = "SELECT d FROM Direccion d WHERE d.noExt = :noExt")
    , @NamedQuery(name = "Direccion.findByNoInt", query = "SELECT d FROM Direccion d WHERE d.noInt = :noInt")
    , @NamedQuery(name = "Direccion.findByColonia", query = "SELECT d FROM Direccion d WHERE d.colonia = :colonia")
    , @NamedQuery(name = "Direccion.findByCp", query = "SELECT d FROM Direccion d WHERE d.cp = :cp")
    , @NamedQuery(name = "Direccion.findByEstatus", query = "SELECT d FROM Direccion d WHERE d.estatus = :estatus")})
public class Direccion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Size(max = 50)
    @Column(name = "calle")
    private String calle;
    @Size(max = 10)
    @Column(name = "no_ext")
    private String noExt;
    @Size(max = 10)
    @Column(name = "no_int")
    private String noInt;
    @Size(max = 50)
    @Column(name = "colonia")
    private String colonia;
    @Size(max = 6)
    @Column(name = "cp")
    private String cp;
    @Column(name = "estatus")
    private Boolean estatus;
    @Column(name = "estado")
    private String estado;
    @Column(name = "municipio")
    private String municipio;
    
    @OneToMany(mappedBy = "idDireccion")
    private List<UsuarioDireccion> usuarioDireccionList;
    @OneToMany(mappedBy = "idDireccion")
    private List<EnvioVenta> envioVentaList;
    @OneToMany(mappedBy = "idDireccion")
    private List<Sucursal> sucursalList;

    public Direccion() {
    }

    public Direccion(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCalle() {
        return calle;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    public String getNoExt() {
        return noExt;
    }

    public void setNoExt(String noExt) {
        this.noExt = noExt;
    }

    public String getNoInt() {
        return noInt;
    }

    public void setNoInt(String noInt) {
        this.noInt = noInt;
    }

    public String getColonia() {
        return colonia;
    }

    public void setColonia(String colonia) {
        this.colonia = colonia;
    }

    public String getCp() {
        return cp;
    }

    public void setCp(String cp) {
        this.cp = cp;
    }

    public Boolean getEstatus() {
        return estatus;
    }

    public void setEstatus(Boolean estatus) {
        this.estatus = estatus;
    }


    @XmlTransient
    public List<UsuarioDireccion> getUsuarioDireccionList() {
        return usuarioDireccionList;
    }

    public void setUsuarioDireccionList(List<UsuarioDireccion> usuarioDireccionList) {
        this.usuarioDireccionList = usuarioDireccionList;
    }

    @XmlTransient
    public List<EnvioVenta> getEnvioVentaList() {
        return envioVentaList;
    }

    public void setEnvioVentaList(List<EnvioVenta> envioVentaList) {
        this.envioVentaList = envioVentaList;
    }

    @XmlTransient
    public List<Sucursal> getSucursalList() {
        return sucursalList;
    }

    public void setSucursalList(List<Sucursal> sucursalList) {
        this.sucursalList = sucursalList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Direccion)) {
            return false;
        }
        Direccion other = (Direccion) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ymp.cupcake.model.entities.Direccion[ id=" + id + " ]";
    }
    
}
