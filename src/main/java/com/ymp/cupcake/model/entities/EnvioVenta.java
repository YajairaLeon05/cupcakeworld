/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ymp.cupcake.model.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author pedro
 */
@Entity
@Table(name = "envio_venta")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "EnvioVenta.findAll", query = "SELECT e FROM EnvioVenta e")
    , @NamedQuery(name = "EnvioVenta.findById", query = "SELECT e FROM EnvioVenta e WHERE e.id = :id")
    , @NamedQuery(name = "EnvioVenta.findByEstatus", query = "SELECT e FROM EnvioVenta e WHERE e.estatus = :estatus")})
public class EnvioVenta implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "estatus")
    private Byte estatus;
    @JoinColumn(name = "id_venta", referencedColumnName = "id")
    @ManyToOne
    private Venta idVenta;
    @JoinColumn(name = "id_direccion", referencedColumnName = "id")
    @ManyToOne
    private Direccion idDireccion;

    public EnvioVenta() {
    }

    public EnvioVenta(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Byte getEstatus() {
        return estatus;
    }

    public void setEstatus(Byte estatus) {
        this.estatus = estatus;
    }

    public Venta getIdVenta() {
        return idVenta;
    }

    public void setIdVenta(Venta idVenta) {
        this.idVenta = idVenta;
    }

    public Direccion getIdDireccion() {
        return idDireccion;
    }

    public void setIdDireccion(Direccion idDireccion) {
        this.idDireccion = idDireccion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EnvioVenta)) {
            return false;
        }
        EnvioVenta other = (EnvioVenta) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ymp.cupcake.model.entities.EnvioVenta[ id=" + id + " ]";
    }
    
}
