/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ymp.cupcake.model.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author pedro
 */
@Entity
@Table(name = "sucursal")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Sucursal.findAll", query = "SELECT s FROM Sucursal s")
    , @NamedQuery(name = "Sucursal.findById", query = "SELECT s FROM Sucursal s WHERE s.id = :id")
    , @NamedQuery(name = "Sucursal.findBySucursalDsc", query = "SELECT s FROM Sucursal s WHERE s.sucursalDsc = :sucursalDsc")
    , @NamedQuery(name = "Sucursal.findByShortDsc", query = "SELECT s FROM Sucursal s WHERE s.shortDsc = :shortDsc")
    , @NamedQuery(name = "Sucursal.findByEstatus", query = "SELECT s FROM Sucursal s WHERE s.estatus = :estatus")})
public class Sucursal implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Size(max = 100)
    @Column(name = "sucursal_dsc")
    private String sucursalDsc;
    @Size(max = 50)
    @Column(name = "short_dsc")
    private String shortDsc;
    @Column(name = "estatus")
    private Boolean estatus;
    @OneToMany(mappedBy = "idSucursal")
    private List<Venta> ventaList;
    @OneToMany(mappedBy = "idSucursal")
    private List<ProductoSucursal> productoSucursalList;
    @OneToMany(mappedBy = "idSucursal")
    private List<UsuarioSucursal> usuarioSucursalList;
    @JoinColumn(name = "id_direccion", referencedColumnName = "id")
    @ManyToOne
    private Direccion idDireccion;

    public Sucursal() {
    }

    public Sucursal(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSucursalDsc() {
        return sucursalDsc;
    }

    public void setSucursalDsc(String sucursalDsc) {
        this.sucursalDsc = sucursalDsc;
    }

    public String getShortDsc() {
        return shortDsc;
    }

    public void setShortDsc(String shortDsc) {
        this.shortDsc = shortDsc;
    }

    public Boolean getEstatus() {
        return estatus;
    }

    public void setEstatus(Boolean estatus) {
        this.estatus = estatus;
    }

    @XmlTransient
    public List<Venta> getVentaList() {
        return ventaList;
    }

    public void setVentaList(List<Venta> ventaList) {
        this.ventaList = ventaList;
    }

    @XmlTransient
    public List<ProductoSucursal> getProductoSucursalList() {
        return productoSucursalList;
    }

    public void setProductoSucursalList(List<ProductoSucursal> productoSucursalList) {
        this.productoSucursalList = productoSucursalList;
    }

    @XmlTransient
    public List<UsuarioSucursal> getUsuarioSucursalList() {
        return usuarioSucursalList;
    }

    public void setUsuarioSucursalList(List<UsuarioSucursal> usuarioSucursalList) {
        this.usuarioSucursalList = usuarioSucursalList;
    }

    public Direccion getIdDireccion() {
        return idDireccion;
    }

    public void setIdDireccion(Direccion idDireccion) {
        this.idDireccion = idDireccion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Sucursal)) {
            return false;
        }
        Sucursal other = (Sucursal) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ymp.cupcake.model.entities.Sucursal[ id=" + id + " ]";
    }
    
}
