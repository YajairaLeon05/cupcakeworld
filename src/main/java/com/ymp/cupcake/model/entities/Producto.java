/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ymp.cupcake.model.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author pedro
 */
@Entity
@Table(name = "producto")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Producto.findAll", query = "SELECT p FROM Producto p")
    , @NamedQuery(name = "Producto.findById", query = "SELECT p FROM Producto p WHERE p.id = :id")
    , @NamedQuery(name = "Producto.findByCodigoBarras", query = "SELECT p FROM Producto p WHERE p.codigoBarras = :codigoBarras")
    , @NamedQuery(name = "Producto.findByProductDsc", query = "SELECT p FROM Producto p WHERE p.productDsc = :productDsc")
    , @NamedQuery(name = "Producto.findByShortDsc", query = "SELECT p FROM Producto p WHERE p.shortDsc = :shortDsc")
    , @NamedQuery(name = "Producto.findByEstatus", query = "SELECT p FROM Producto p WHERE p.estatus = :estatus")})
public class Producto implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Size(max = 50)
    @Column(name = "codigo_barras")
    private String codigoBarras;
    @Size(max = 100)
    @Column(name = "product_dsc")
    private String productDsc;
    @Size(max = 100)
    @Column(name = "short_dsc")
    private String shortDsc;
    @Column(name = "estatus")
    private Boolean estatus;
    @OneToMany(mappedBy = "idProducto")
    private List<ProductoSucursal> productoSucursalList;
    @JoinColumn(name = "id_subcategoria", referencedColumnName = "id")
    @ManyToOne
    private Subcategoria idSubcategoria;
    @OneToMany(mappedBy = "idProducto")
    private List<DetalleVenta> detalleVentaList;

    public Producto() {
    }

    public Producto(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCodigoBarras() {
        return codigoBarras;
    }

    public void setCodigoBarras(String codigoBarras) {
        this.codigoBarras = codigoBarras;
    }

    public String getProductDsc() {
        return productDsc;
    }

    public void setProductDsc(String productDsc) {
        this.productDsc = productDsc;
    }

    public String getShortDsc() {
        return shortDsc;
    }

    public void setShortDsc(String shortDsc) {
        this.shortDsc = shortDsc;
    }

    public Boolean getEstatus() {
        return estatus;
    }

    public void setEstatus(Boolean estatus) {
        this.estatus = estatus;
    }

    @XmlTransient
    public List<ProductoSucursal> getProductoSucursalList() {
        return productoSucursalList;
    }

    public void setProductoSucursalList(List<ProductoSucursal> productoSucursalList) {
        this.productoSucursalList = productoSucursalList;
    }

    public Subcategoria getIdSubcategoria() {
        return idSubcategoria;
    }

    public void setIdSubcategoria(Subcategoria idSubcategoria) {
        this.idSubcategoria = idSubcategoria;
    }

    @XmlTransient
    public List<DetalleVenta> getDetalleVentaList() {
        return detalleVentaList;
    }

    public void setDetalleVentaList(List<DetalleVenta> detalleVentaList) {
        this.detalleVentaList = detalleVentaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Producto)) {
            return false;
        }
        Producto other = (Producto) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ymp.cupcake.model.entities.Producto[ id=" + id + " ]";
    }
    
}
