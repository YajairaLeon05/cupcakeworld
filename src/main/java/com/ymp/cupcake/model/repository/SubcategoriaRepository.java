/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ymp.cupcake.model.repository;

import com.ymp.cupcake.model.entities.Subcategoria;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author pedro
 */
public interface SubcategoriaRepository extends CrudRepository<Subcategoria, Number> {
    
    public Iterable<Subcategoria> findAllByIdCategoriaFkEquals(Number id);
    
}
