/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ymp.cupcake.model.repository;

import com.ymp.cupcake.model.entities.Usuario;
import java.util.Optional;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author pedro
 */
public interface UsuarioRepository extends CrudRepository<Usuario, Number> {
    
    public Optional<Usuario> findByUsernameEquals(String username);
    
}
