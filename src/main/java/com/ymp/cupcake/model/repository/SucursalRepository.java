/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ymp.cupcake.model.repository;

import org.springframework.data.repository.CrudRepository;
import com.ymp.cupcake.model.entities.Sucursal;

/**
 *
 * @author pedro
 */
public interface SucursalRepository extends CrudRepository<Sucursal, Number> {
    
}
