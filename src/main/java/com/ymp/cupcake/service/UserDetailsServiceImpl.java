package com.ymp.cupcake.service;

import com.ymp.cupcake.model.business.UsuarioBusiness;
import com.ymp.cupcake.model.entities.Usuario;
import com.ymp.cupcake.model.repository.UsuarioRepository;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    
    @Autowired
    private UsuarioBusiness usuarioBusiness;

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        
        Usuario usuarioEntity = usuarioBusiness.buscarUsuario(userName);


        if (usuarioEntity == null) {
            System.out.println("Usuario no encontrado! " + userName);
            throw new UsernameNotFoundException("Usuario " + userName + " no fue encontrado en la base de datos");
        }

        System.out.println("Usuario: " + usuarioEntity);
        
        List<String> roleNames = new ArrayList<>();
        roleNames.add(usuarioEntity.getTipoUsuario());

        List<GrantedAuthority> grantList = new ArrayList<GrantedAuthority>();
        for (String role : roleNames) {
            GrantedAuthority authority = new SimpleGrantedAuthority(role);
            grantList.add(authority);
        }

        UserDetails userDetails = (UserDetails) new User(usuarioEntity.getUsername(),
                usuarioEntity.getPassword(), grantList);
        return userDetails;
    }

}