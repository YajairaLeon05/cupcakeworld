<html xmlns:th="http://www.thymeleaf.org">
<head>
    <meta charset="utf-8">
    <!--[if IE]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Page title -->
    <title>Login</title>
    <!--[if lt IE 9]>
    <script th:src="@{/recursos/main/js/respond.js}"></script>
    <![endif]-->
    <!-- Bootstrap Core CSS -->
    <link link th:href="@{/recursos/main/css/bootstrap.css}" rel="stylesheet" type="text/css">
    <!-- Icon fonts -->
    <link link th:href="@{/recursos/main/fonts/font-awesome/css/font-awesome.min.css}" rel="stylesheet" type="text/css">
    <link link th:href="@{/recursos/main/fonts/flaticons/flaticon.css}" rel="stylesheet" type="text/css">
    <link link th:href="@{/recursos/main/fonts/glyphicons/bootstrap-glyphicons.css}" rel="stylesheet" type="text/css">
    <!-- Google fonts -->
    <link href="https://fonts.googleapis.com/css?family=Karla:400,600,700%7CCherry+Swash:400,700" rel="stylesheet">
    <!-- Style CSS -->
    <link link th:href="@{/recursos/main/css/style.css}" rel="stylesheet">
    <!-- Color Style CSS -->
    <link link th:href="@{/recursos/main/styles/maincolors.css}" rel="stylesheet">
    <!-- CSS Plugins -->
    <link rel="stylesheet" link th:href="@{/recursos/main/css/plugins.css}" >
    <!-- LayerSlider CSS -->
    <link rel="stylesheet" link th:href="@{/recursos/main/js/layerslider/css/layerslider.css}">
    <!-- Favicons-->
    <link rel="apple-touch-icon" sizes="72x72" th:href="@{/recursos/main/apple-icon-72x72.png}">
    <link rel="apple-touch-icon" sizes="114x114"  th:href="@{/recursos/main/apple-icon-114x114.png}">
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
</head>
<body style="background-image: url('http://localhost:8080/recursos/main/css/../img/caketopbg.png')">

<div class="container-fluid row margin1" style="margin-top: 20rem; margin-left: 33%; max-width: 33%">
    <div class="col-md-12">
        <h4>Iniciar Sesión</h4>
        <p>Por favor, identificate para acceder a este sitio</p>
        <form class="form-style" id="login_form" th:action="@{/Entrar}">
            <div class="form-group">
                <label>Correo<span class="required">*</span></label>
                <input type="email" name="usuario" class="form-control input-field" placeholder="Usuario" required>
                <label>Contraseña</label>
                <input type="password" name="contrasena" class="form-control input-field" placeholder="Contraseña">
                <button type="submit" class="blob-btn" id="submit_btn" style="width: 100%">
                    Entrar
                    <span class="blob-btn__inner">
                        <span class="blob-btn__blobs">
                        <span class="blob-btn__blob"></span>
                        <span class="blob-btn__blob"></span>
                        <span class="blob-btn__blob"></span>
                        <span class="blob-btn__blob"></span>
                        </span>
                        </span>
                </button>
            </div>
        </form>
    </div>
</div>

<script th:src="@{/recursos/main/js/jquery.min.js}"></script>
<script th:src="@{/recursos/main/js/bootstrap.min.js}"></script>
<!-- Main Js -->
<script th:src="@{/recursos/main/js/main.js}"></script>
<!-- Open street maps -->
<script th:src="@{/recursos/main/js/map.js}"></script>
<!-- MailChimp Validator -->
<script th:src="@{/recursos/main/js/mc-validate.js}"></script>
<!-- GreenSock -->
<script th:src="@{/recursos/main/js/layerslider/js/greensock.js}"></script>
<!-- LayerSlider Script files -->
<script th:src="@{/recursos/main/js/layerslider/js/layerslider.transitions.js}"></script>
<script th:src="@{/recursos/main/js/layerslider/js/layerslider.kreaturamedia.jquery.js}"></script>
<script th:src="@{/recursos/main/js/layerslider/js/layerslider.load.js}"></script>
<!-- Other Plugins -->
<script th:src="@{/recursos/main/js/plugins.js}"></script>
<!-- Prefix Free CSS -->
<script th:src="@{/recursos/main/js/prefixfree.js}"></script>
<!-- Counter -->
<script th:src="@{/recursos/main/js/counter.js}"></script>
</body>
</html>